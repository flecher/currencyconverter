package com.kanzi.currencyconverter.utilities.extensions

import android.content.Context
import com.kanzi.currencyconverter.MainApp
import com.kanzi.currencyconverter.di.component.AppComponent

fun Context.getAppComponent(): AppComponent = (applicationContext as MainApp).appComponent
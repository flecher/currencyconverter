package com.kanzi.currencyconverter

import android.app.Application
import com.kanzi.currencyconverter.di.component.AppComponent
import com.kanzi.currencyconverter.di.component.DaggerAppComponent

class MainApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        setupComponent()
    }

    private fun setupComponent() {
        appComponent = DaggerAppComponent.builder().build()
    }
}

package com.kanzi.currencyconverter.data

import com.kanzi.currencyconverter.data.model.ConvertionResponse
import io.reactivex.Observable

interface DataManager {

    fun getConvertionResults(fromAmount: Double,
                             fromCurrency: String,
                             toCurrency: String): Observable<ConvertionResponse>
}
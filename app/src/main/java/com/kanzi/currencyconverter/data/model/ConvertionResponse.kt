package com.kanzi.currencyconverter.data.model

data class ConvertionResponse(val amount: Double,
                              val currency: String)
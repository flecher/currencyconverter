package com.kanzi.currencyconverter.data.model

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Singleton

@Singleton
internal class CurrencyRepository(context: Context) {

    val PREFS_NAME = "currencies"
    val EUR_CURRENCY = "eur"
    val USD_CURRENCY = "usd"
    val JPY_CURRENCY = "jpy"

    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    var eur: Double
        get() = prefs.getString(EUR_CURRENCY, "1000.0").toDouble()
        set(value) = prefs.edit().putString(EUR_CURRENCY, value.toString()).apply()

    var usd: Double
        get() = prefs.getString(USD_CURRENCY, "0.0").toDouble()
        set(value) = prefs.edit().putString(USD_CURRENCY, value.toString()).apply()

    var jpy: Double
        get() = prefs.getString(JPY_CURRENCY, "0.0").toDouble()
        set(value) = prefs.edit().putString(JPY_CURRENCY, value.toString()).apply()
}
package com.kanzi.currencyconverter.data.remote

import com.kanzi.currencyconverter.data.model.ConvertionResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path


interface ConvertService {

    @GET("{fromAmount}-{fromCurrency}/{toCurrency}/latest")
    fun convertValue(@Path("fromAmount") fromAmount: Double,
                     @Path("fromCurrency") fromCurrency: String,
                     @Path("toCurrency") toCurrency: String): Observable<ConvertionResponse>

}
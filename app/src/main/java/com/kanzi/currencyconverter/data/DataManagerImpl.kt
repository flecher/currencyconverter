package com.kanzi.currencyconverter.data

import com.kanzi.currencyconverter.data.model.ConvertionResponse
import com.kanzi.currencyconverter.data.remote.ConvertService
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class DataManagerImpl @Inject constructor(private val convertService: ConvertService) : DataManager {

    override fun getConvertionResults(fromAmount: Double,
                                      fromCurrency: String,
                                      toCurrency: String): Observable<ConvertionResponse> {
        return convertService.convertValue(fromAmount, fromCurrency, toCurrency)
                .flatMap { Observable.just(it) }
    }

}
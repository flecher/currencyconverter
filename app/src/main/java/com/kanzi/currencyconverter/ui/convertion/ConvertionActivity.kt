package com.kanzi.currencyconverter.ui.convertion

import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import com.kanzi.currencyconverter.R
import com.kanzi.currencyconverter.ui.base.BaseActivity
import kotlinx.android.synthetic.main.convertion_main.*
import javax.inject.Inject


class ConvertionActivity : BaseActivity(), ConvertionView {

    companion object {
        val TAG = ConvertionActivity::class.java.simpleName
    }

    @Inject
    lateinit var presenter: ConvertionPresenter

    override fun getLayoutResId() = R.layout.convertion_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityComponent?.inject(this)

        presenter.bind(this)

        setUp()
    }

    fun setUp() {
        updateCurrentCurrencies()
        presenter.setUpSpinner(this)
        confirm_convertion_button.setOnClickListener {
            doMoneyConvertion()
        }
    }

    override fun onFetchConvertionSuccess() {
        updateCurrentCurrencies()
    }

    override fun onFetchError(error: Throwable) {
        Log.e(TAG, "Failure fetching data", error)
        Toast.makeText(this, "Failure fetching data", Toast.LENGTH_LONG).show()
    }

    override fun doMoneyConvertion() {
        presenter.test(
                from_currency_spinner.selectedItem.toString(),
                to_currency_spinner.selectedItem.toString(),
                amount_text.text.toString().toDouble())
    }

    override fun setUpSpinner(spinnerAdapter: ArrayAdapter<String>) {
        to_currency_spinner!!.adapter = spinnerAdapter
        from_currency_spinner!!.adapter = spinnerAdapter
    }

    private fun updateCurrentCurrencies() {
        presenter.requestCurrencyUpdate()
    }

    override fun addLogToReport(arrayList: ArrayList<String>) {
        val adapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1, arrayList)
        log_list_view!!.adapter = adapter
        adapter.notifyDataSetChanged()
        log_list_view!!.invalidate()
    }

    override fun showMessage(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }

    override fun updateCurrency(eur: Double, usd: Double, jpy: Double) {
        EUR_currency.text = String.format("%.2f", eur)
        USD_currency.text = String.format("%.2f", usd)
        JPY_currency.text = String.format("%.2f", jpy)
    }

}
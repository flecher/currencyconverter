package com.kanzi.currencyconverter.ui.convertion

import android.content.Context
import android.widget.ArrayAdapter
import com.kanzi.currencyconverter.data.DataManager
import com.kanzi.currencyconverter.data.model.ConvertionResponse
import com.kanzi.currencyconverter.di.scope.PerView
import com.kanzi.currencyconverter.ui.base.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@PerView
class ConvertionPresenter @Inject constructor(private val dataManager: DataManager) : BasePresenter<ConvertionView>() {

    companion object {
        val TAG = ConvertionPresenter::class.java.simpleName
        val EUR = "EUR"
        val USD = "USD"
        val JPY = "JPY"
    }

    private var commisionCount = 0
    private var addCommision = false
    private var EUR_CURRENCY: Double = 1000.0
    private var USD_CURRENCY: Double = 0.0
    private var JPY_CURRENCY: Double = 0.0
    private val reportArray: ArrayList<String> = ArrayList<String>()

    fun requestCurrencyUpdate() {
        view?.updateCurrency(EUR_CURRENCY, USD_CURRENCY, JPY_CURRENCY)
    }

    fun setUpSpinner(context: Context) {
        val spinnerArray: ArrayList<String> = ArrayList<String>()
        spinnerArray.add(EUR)
        spinnerArray.add(USD)
        spinnerArray.add(JPY)
        val sAdapter: ArrayAdapter<String> = ArrayAdapter(
                context,
                android.R.layout.simple_spinner_item,
                spinnerArray)
        sAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        view?.setUpSpinner(sAdapter)
    }

    fun test(fromCurrency: String,
             toCurrency: String,
             amount: Double) {

        if (confirmConvertableAmount(fromCurrency, amount)) {
            fetchConvertionResults(fromCurrency, toCurrency, amount)
        } else {
            view?.showMessage("Jums trūksta pinigų!")
        }
    }

    private fun fetchConvertionResults(fromCurrency: String,
                                       toCurrency: String,
                                       amount: Double) {

        disposables.add(
                dataManager.getConvertionResults(
                        amount,
                        fromCurrency,
                        toCurrency)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { onFetchConvertionSuccess(it, amount, fromCurrency) },
                                { onFetchError(it) }
                        )
        )
    }

    private fun onFetchConvertionSuccess(convertionResponse: ConvertionResponse,
                                         convertedAmount: Double,
                                         fromCurrency: String) {
        commisionCount++
        handleResponse(convertionResponse, convertedAmount, fromCurrency)
        view?.onFetchConvertionSuccess()
    }

    private fun onFetchError(error: Throwable) {
        view?.onFetchError(error)
    }

    private fun confirmConvertableAmount(fromCurrency: String,
                                         amount: Double): Boolean {

        var availabeToConvert = false

        if (fromCurrency.equals("EUR")) {
            availabeToConvert = (EUR_CURRENCY - amount) > 0
        }

        if (fromCurrency.equals("USD")) {
            availabeToConvert = (USD_CURRENCY - amount) > 0
        }

        if (fromCurrency.equals("JPY")) {
            availabeToConvert = (JPY_CURRENCY - amount) > 0
        }
        return availabeToConvert
    }

    private fun handleResponse(convertionResponse: ConvertionResponse,
                               convertedAmount: Double,
                               fromCurrency: String) {

        var amountToSubstract = convertedAmount

        if (commisionCount > 3) {
            addCommision = true
        }

        if (addCommision) {
            amountToSubstract += 0.7
            addLogToReport("Jūs konvertavote " + convertedAmount + " " + fromCurrency + " į " + convertionResponse.amount + " " + convertionResponse.currency + ". Komisinis mokestis - 0.70 EUR.")
        } else {
            addLogToReport("Jūs konvertavote " + convertedAmount + " " + fromCurrency + " į " + convertionResponse.amount + " " + convertionResponse.currency + ". Komisinis mokestis - 0.00 EUR.")
        }

        when (convertionResponse.currency) {
            "EUR" -> EUR_CURRENCY += convertionResponse.amount
            "USD" -> USD_CURRENCY += convertionResponse.amount
            "JPY" -> JPY_CURRENCY += convertionResponse.amount
            else -> {
                view?.showMessage("Klaida")
            }
        }

        when (fromCurrency) {
            "EUR" -> EUR_CURRENCY -= amountToSubstract
            "USD" -> USD_CURRENCY -= amountToSubstract
            "JPY" -> JPY_CURRENCY -= amountToSubstract
            else -> {
                view?.showMessage("Klaida")
            }
        }
    }

    private fun addLogToReport(text: String) {
        reportArray.add(text)
        view?.addLogToReport(reportArray)
    }

}
package com.kanzi.currencyconverter.ui.convertion

import android.widget.ArrayAdapter
import com.kanzi.currencyconverter.ui.base.MvpView

interface ConvertionView : MvpView {
    fun onFetchConvertionSuccess()

    fun onFetchError(error: Throwable)

    fun doMoneyConvertion()

    fun setUpSpinner(spinnerAdapter: ArrayAdapter<String>)

    fun addLogToReport(arrayList: ArrayList<String>)

    fun showMessage(text: String)

    fun updateCurrency(eur: Double,
                       usd: Double,
                       jpy: Double)
}
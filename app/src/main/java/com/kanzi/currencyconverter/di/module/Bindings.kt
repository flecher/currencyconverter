package com.kanzi.currencyconverter.di.module

import com.kanzi.currencyconverter.data.DataManager
import com.kanzi.currencyconverter.data.DataManagerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class Bindings {

    @Binds
    internal abstract fun bindDataManager(manager: DataManagerImpl): DataManager
}
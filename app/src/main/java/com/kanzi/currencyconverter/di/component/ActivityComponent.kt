package com.kanzi.currencyconverter.di.component

import android.content.Context
import com.kanzi.currencyconverter.di.scope.PerView
import com.kanzi.currencyconverter.ui.convertion.ConvertionActivity
import dagger.Subcomponent

@PerView
@Subcomponent
interface ActivityComponent {

    fun inject(activity: ConvertionActivity)
}
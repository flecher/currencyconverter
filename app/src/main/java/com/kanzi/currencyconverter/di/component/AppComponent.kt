package com.kanzi.currencyconverter.di.component

import com.kanzi.currencyconverter.di.module.Bindings
import com.kanzi.currencyconverter.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(Bindings::class), (NetworkModule::class)])
interface AppComponent {
    fun activityComponent(): ActivityComponent
}